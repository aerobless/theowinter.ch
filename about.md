---
layout: page
permalink: /about/
title: Hello
tags: [about, Theo Winter, website]
description: Theo Winter is studying Computer Science at Hochschule für Technik Rapperswil in Switzerland. Some of his hobbies are programming, photography, cycling and more.
modified: 2014-03-26
image:
  feature: switzerland.jpg
---


My name is Theo Winter and I'm studying computer science at [HSR](http://www.hsr.ch){:target="_blank"} in [Switzerland](https://www.google.com/maps/place/Switzerland){:target="_blank"}.
Some of my hobbies are programming, [photography]({{ site.url }}/photography/), cycling, skiing and more. My tools of choice are OS X,
Canon, GitHub and everything from Google.
I'm levelheaded and I don't participate in flamewars. I believe in open source and I've made most of my projects freely available on
[GitHub](https://github.com/aerobless){:target="_blank"}. Yes, you can find the repository of this site on GitHub as well.  
If you'd like to know more feel free to contact me or visit my
[G+ profile](https://plus.google.com/+TheoWinterCH){:target="_blank"}.

This page is my personal blog and I use it as a place to show off some of my projects. Enjoy :)

## Skills
 + **Programming Languages:** Java, Go, C# & .NET, JavaScript
    + **Supporting Skills:** Continuous Integration, Test-Driven Development, Patterns of Enterprise Application Architecture,
     Enterprise Integration Patterns, GoF Patterns
 + **Web:** HTML, CSS, JavaScript, WordPress, Jekyll
 + **Graphics:** Adobe Photoshop, Adobe Lightroom
 + **Databases:** Postgres, Oracle, MSSQL
 + **Cloud Services & Servers:** Amazon AWS, Digital Ocean, OVH
 + **Operating Systems:** OS X, Ubuntu, Windows

## Contact
Feel free to send me an email at [theo@w1nter.com](mailto://theo@w1nter.com) or chat with me on [Google Hangouts](https://plus.google.com/+TheoWinterCH){:target="_blank"}.
If you want to send me a file you can drop it in the marked area below.

<link rel="stylesheet" type="text/css" href="{{site.url}}/css/dropzone.css">
<script src="{{ site.url }}/js/vendor/dropzone.js"></script>
<form class="dropzone" action="{{ site.url }}/upload.php" class="dropzone"></form>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Person",
  "name": "Theo Winter",
  "givenName": "Theo",
  "familyName": "Winter",
  "alternateName": "Theodor Winter",
  "description": "Theo Winter is studying Computer Science at Hochschule für Technik Rapperswil in Switzerland. Some of his hobbies are programming, photography, cycling and more.",
  "gender": "male",
  "nationality": "Switzerland",
  "image": "{{ site.url }}/images/bio-photo.png",
  "jobTitle": "Software Developer",
  "email": "mailto:theo@w1nter.com",
  "sameAs": "https://plus.google.com/+TheoWinterCH",
  "url": "http://www.theowinter.ch"
}
</script>

{% include toc.html %}
